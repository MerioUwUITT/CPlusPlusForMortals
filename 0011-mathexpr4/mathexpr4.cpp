/*
programa para resolver la siguiente expresion matematica
(a + (b/(c-d)))
*/

#include<iostream>
using namespace std;

int main(int argc, char const *argv[]) {
  /* code */
  float a, b, c, d, response = 0;
  std::cout << "Digita el valor del parametro a: " << '\n';std::cin >> a;
  std::cout << "Digita el valor del parametro b: " << '\n';std::cin >> b;
  std::cout << "Digita el valor del parametro c: " << '\n';std::cin >> c;
  std::cout << "Digita el valor del parametro d: " << '\n';std::cin >> d;

  response = a + (b/(c-d));

  std::cout << "El resultado de la expresion matematica es: " << response << '\n';

  return 0;
}
