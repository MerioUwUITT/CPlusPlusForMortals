/*
  type_data.cpp
  programa que nos ayuda a conocer los tipos
  de datos primitivos en C++
*/

#include<iostream>
using namespace std;


int main(int argc, char const *argv[]) {
  int num = 10;
  float fnum = 2.89;
  string s = "string";
  char c = 'a';

  std::cout << "Salida de los tipos de datos Primitivos: " << '\n';
  std::cout << "Tipo Numerico Entero: " << num << '\n';
  std::cout << "Tipo Numerico Flotante: " << fnum << '\n';
  std::cout << "Tipo String: " << s << '\n';
  std::cout << "Tipo Caracter: " << c << '\n';

  return 0;
}
